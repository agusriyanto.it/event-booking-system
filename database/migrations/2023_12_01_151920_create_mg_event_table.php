<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mg_event', function (Blueprint $table) {
            $table->id('evn_id');
            $table->string('evn_title');
            $table->string('evn_description');
            $table->date('env_date');
            $table->time('evn_time');
            $table->string('evn_location');
            $table->string('evn_price');
            $table->string('evn_slotavaiable');
            $table->integer('evn_author');
            $table->integer('evn_sts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mg_event');
    }
};
