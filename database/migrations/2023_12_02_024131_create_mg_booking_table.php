<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mg_booking', function (Blueprint $table) {
            $table->id('booking_id');
            $table->string('booking_code')->unique();
            $table->integer('user_id');
            $table->integer('evn_id');
            $table->dateTime('booking_expired');
            $table->integer('booking_sts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mg_booking');
    }
};
