<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Event\EventController;
use App\Http\Controllers\Booking\BookingController;
use App\Http\Controllers\ProfieController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware('isLogin');

Route::get('auth', [LoginController::class,'index'])->name('auth')->middleware('isGuest');
Route::get('register', [LoginController::class,'getRegister'])->name('register')->middleware('isGuest');
Route::post('/login-action', [LoginController::class,'login'])->middleware('isGuest');
Route::post('/register-action',[LoginController::class,'register'])->middleware('isGuest');
Route::get('logout', [LoginController::class,'logout']);

Route::get('dashboard', [DashboardController::class,'index'])->middleware('isLogin');

Route::resource('event', EventController::class)->middleware('isLogin');
Route::resource('book', BookingController::class)->middleware('isLogin');
Route::resource('profile', ProfieController::class)->middleware('isLogin');
Route::get('pay/{id}', [BookingController::class,'getPay'])->name('confrm-pay')->middleware('isLogin');
Route::get('secure', [ProfieController::class,'getSecure'])->name('reset-password')->middleware('isLogin');
Route::post('/secure-action', [ProfieController::class,'resetPassword'])->middleware('isLogin');

Route::get('/resend', [LoginController::class,'resendMail'])->name('resend-mail');
Route::get('/inactive', [LoginController::class,'inActive'])->name('inactive_user_route');
Route::get('/check-code-booking/{id}', [BookingController::class,'checkCode'])->name('check-code')->middleware('isGuest');
