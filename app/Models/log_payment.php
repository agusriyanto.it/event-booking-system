<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class log_payment extends Model
{
    use HasFactory;
    protected $table = "log_payment";
    protected $fillable = [
        'booking_id',
    ];
}
