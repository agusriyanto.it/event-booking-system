<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mg_event extends Model
{
    use HasFactory;
    protected $table = "mg_event";
    protected $fillable = [
        'evn_title',
        'evn_description',
        'env_date',
        'evn_time',
        'evn_location',
        'evn_price',
        'evn_slotavaiable',
        'evn_author'
    ];
}
