<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mg_booking extends Model
{
    use HasFactory;
    protected $table = "mg_booking";
    protected $fillable = [
        'booking_code',
        'user_id',
        'evn_id',
        'booking_expired',
        'booking_sts',
    ];
}
