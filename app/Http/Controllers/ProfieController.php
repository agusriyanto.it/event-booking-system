<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Mg_event;
use App\Models\Mg_booking;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
Use Alert;

class ProfieController extends Controller
{

    function index(){
        $data['title'] = "Profile | Booking App";
        $data['user'] = user::where('id', auth()->user()->id)->first();  
        return view('profile.index')->with('data',$data);
    }

    function store(Request $request){
        $request->validate([
            'name'      => 'required|min:3|max:100|regex:/^[\p{L}\p{M}\s.\-]+$/u',
            'email'     => 'required|email',
            'phone'     => 'required|numeric|min:12',
            'gender'    => 'required',            
            'address'   => 'required|max:100'
        ]);

        if (!preg_match('/^[a-zA-Z\s]+$/', $request->name)) {
            return redirect()->back()->with('err', 'Name is invalid. Do not use numbers and symbols');
        }

        $mailVerify = explode("@", $request->email);
        if (!in_array($mailVerify[1], array("gmail.com", "yahoo.com", "yahoo.co.id", "icloud.com", "outlook.com", "ymail.com", "hotmail.com", "rocketmail.com"))) {
            return redirect()->back()->with('err', 'Please check your email. make sure to follow the provisions');
        }
        
        if (substr($request->phone, 0, 3) != 628) {
            return redirect()->back()->with('err', 'Invalid Phone Number. Use prefix 62 !');
        }

        User::where('id', auth()->user()->id)->update([
            'name'      => $request->name,
            'email'     => $request->email,
            'usr_phone' => $request->phone,
            'usr_address'   =>$request->address,
            'usr_gender'    =>$request->gender
        ]);
        return redirect('profile')->with('toast_success','Successfuly update profile');
    }

    function getSecure(){
        $data['title'] = "Reset Password | Booking App";
        $data['user'] = user::where('id', auth()->user()->id)->first();  
        return view('profile.secure')->with('data',$data);
    }

    function resetPassword(Request $request){
        $request->validate([
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        User::where('id', auth()->user()->id)->update(['password' => Hash::make($request->password)]);
        return redirect('profile')->with('toast_success','Successfuly reset password');
    }
}
