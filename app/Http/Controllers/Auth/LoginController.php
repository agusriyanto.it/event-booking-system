<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct(){
        // $this->middleware('auth:api');
    }

    public function index(){
        $data['title'] = "Login | Booking App";
        return view('auth.login')->with('data',$data);
    }

    public function login(Request $request){
        $request->validate([
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if (auth()->attempt(request(['email', 'password']))) {
            return redirect('dashboard')->with('toast_success','Successfuly Login');
        }else{
            return redirect('auth')->with('errorlogin','Login failed, please check your email and password correctly');
        }
    }

    public function getRegister(){
        $data['title'] = "Register | Booking App"; 
        return view('auth.register')->with('data',$data);
    }
    
    public function register(Request $request){
        Session::flash('name',$request->name);
        Session::flash('role',$request->role);
        Session::flash('email',$request->email);
        Session::flash('password',$request->password);
        $request->validate([
            'name'      => 'required',
            'role'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6'
        ]);

        if (!preg_match('/^[a-zA-Z\s]+$/', $request->name)) {
            return redirect()->back()->with('err', 'Name is invalid. Do not use numbers and symbols');
        }

        $mailVerify = explode("@", $request->email);
        if (!in_array($mailVerify[1], array("gmail.com", "yahoo.com", "yahoo.co.id", "icloud.com", "outlook.com", "ymail.com", "hotmail.com", "rocketmail.com"))) {
            return redirect()->back()->with('err', 'Please check your email. make sure to follow the provisions');
        }
        
        User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'role'      => $request->role,
            'password'  => Hash::make($request->password)
        ]);
        if (auth()->attempt(request(['email', 'password']))) {
            return redirect('dashboard')->with('toast_success','Successfuly Login');
        }else{
            return 'ada';
        }
    }   

    public function logout(){
        auth()->logout();
        return redirect('auth');
    }

    function inActive(){
        if(Auth::user()){
            if(Auth::user()->usr_sts == 1){
                $data['title'] = "Inactive Account | Booking App"; 
                return view('auth.inactive')->with('data',$data);
            }else{
                return redirect('auth');
            }
        }
        return redirect('auth');
    }

    function resendMail (){
        User::where('id', auth()->user()->id)->update(['usr_sts' => 2]);
        return redirect('profile')->with('toast_success','Congratulation your account is active');
    }
}
