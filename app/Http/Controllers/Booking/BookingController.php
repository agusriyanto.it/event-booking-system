<?php

namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Mg_booking;
use App\Models\Mg_event;
use App\Models\log_payment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;

class BookingController extends Controller
{
    function index(){
        $data['title'] = "List Booking | Booking App";
        if (auth()->user()->role == 1) { // role 1 as Administrator
            $data['a1'] = DB::table('mg_booking as b')
                        ->join('mg_event as e', 'e.evn_id', '=', 'b.evn_id')
                        ->join('users as u', 'u.id', '=', 'b.user_id')
                        ->paginate(10);
        }else{
            $data['a1'] = DB::table('mg_booking as b')
                        ->join('mg_event as e', 'e.evn_id', '=', 'b.evn_id')
                        ->where('user_id', auth()->user()->id)
                        ->paginate(10);
        }
        return view('booking.index')->with('data',$data);
    }

    function show($id){
        $data['title'] = "Detail Booking | Booking App";
        $data['a1'] = DB::table('mg_booking as b')
                        ->join('mg_event as e', 'e.evn_id', '=', 'b.evn_id')
                        ->join('users as u', 'u.id', '=', 'b.user_id')
                        ->where('b.booking_id', Crypt::decrypt($id))
                        ->first();
        return view('booking.detail')->with('data',$data);
    }

    function edit($id){
        $data['title'] = "Booking Event | Booking App";
        $getAvl = mg_booking::where('evn_id',Crypt::decrypt($id))->get()->count();  
        $data['checkuser'] = DB::table('mg_booking as b')
                        ->where('evn_id',Crypt::decrypt($id))
                        ->where('user_id', auth()->user()->id)
                        ->get()->count();
        $data['a1'] = mg_event::where('evn_id',Crypt::decrypt($id))->first();
        if($getAvl == $data['a1']->evn_slotavaiable){
            $data['check'] = 1; //full booked
        }else{
            $data['check'] = 2;
        }  
        if($data['a1']->evn_sts == 1){
            $data['exp'] = 1; //Running event
        }else{
            $data['exp'] = 2; //expired event
        }       
        return view('booking.add')->with('data',$data);
    }

    function store(Request $request){
        $request->validate([
            'id'      => 'required',
        ]);

        $booking = Mg_booking::create([
            'booking_code' => Str::random(8),
            'user_id' => auth()->user()->id,
            'evn_id' => $request->id,
            'booking_sts' => 1,
        ]);
        $envprice = DB::table('mg_event')->where('evn_id', $request->id)->first();

        return redirect('book')->with('toast_success','Successfuly Booking Event ' . $request->title);
    }

    function getPay($id){
        $data['title'] = "Confirmation Payment | Booking App";
        $data['a1'] = DB::table('mg_booking as b')
                        ->join('mg_event as e', 'e.evn_id', '=', 'b.evn_id')
                        ->join('users as u', 'u.id', '=', 'b.user_id')
                        ->where('b.booking_id', Crypt::decrypt($id))
                        ->first();
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('midtrans.server_key');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id' => $data['a1']->booking_id,
                'gross_amount' => $data['a1']->evn_price,
            ),
            'customer_details' => array(
                'first_name' => auth()->user()->name,
            ),
        );
        $snapToken = \Midtrans\Snap::getSnapToken($params);
        return view('booking.confrmpay')->with(['data'=>$data,'snapToken'=>$snapToken]);
    }

    function midtransCallback(Request $request){
        $serverKey = config('midtrans.server_key');
        $hashed = hash("sha512",$request->order_id.$request->status_code.$request->gross_amount.$serverKey);
        if($hashed == $request->signature_key){
            if($request->transaction_status == "capture" || $request->transaction_status == "settlement"){
                Mg_booking::where('booking_id',$request->order_id)->update([
                    'booking_sts' => 2
                ]);
                log_payment::create([
                    'booking_id' => $request->order_id
                ]);
            }
        }

    }

    function checkCode($id){
        $data['title'] = "Checkout | Booking App";
        $data['a1'] = DB::table('mg_booking as b')
                        ->join('mg_event as e', 'e.evn_id', '=', 'b.evn_id')
                        ->join('users as u', 'u.id', '=', 'b.user_id')
                        ->where('b.booking_code', $id)
                        ->first();
        if(!empty($data['a1'])){
            return view('booking.found')->with('data',$data);
        }else{
            return redirect('/');
        }
        
    }
}
