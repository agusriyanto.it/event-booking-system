<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Mg_event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
Use Alert;

class EventController extends Controller
{
    function index($id=null){
        $data['title'] = "Create Event | Booking App";
        return view('event.add')->with('data',$data);
    }

    function show($id){
        $data['title'] = "Detail Event | Booking App";
        $data['a1'] = mg_event::where('evn_id',Crypt::decrypt($id))->first();
        return view('event.detail')->with('data',$data);
    }

    function store(Request $request){ 
        $request->validate([
            'title'      => 'required',
            'description'      => 'required',
            'date'     => 'required',
            'location'     => 'required',
            'time'  => 'required',
            'avaliable'  => 'required',
            'price' => 'required'
        ]);

        Mg_event::create([
            'evn_title' => $request->title,
            'evn_description' => $request->description,
            'env_date' => $request->date,
            'evn_time' => $request->time,
            'evn_location' => $request->location,
            'evn_slotavaiable' => $request->avaliable,
            'evn_price' => $request->price,
            'evn_author' => auth()->user()->id,
        ]);
        return redirect('dashboard')->with('toast_success','Successfuly Create Event ' . $request->title);
    }

    function edit($id){
        $data['title'] = "Edit Event | Booking App";
        $data['a1'] = mg_event::where('evn_id',Crypt::decrypt($id))->first();
        return view('event.edit')->with('data',$data);
    }

    function update(Request $request, $id){
        $request->validate([
            'title'      => 'required',
            'description'      => 'required',
            'date'     => 'required',
            'location'     => 'required',
            'time'  => 'required',
            'avaliable'  => 'required',
            'price' => 'required'
        ]);

        mg_event::where('evn_id',$id)->update([
            'evn_title' => $request->title,
            'evn_description' => $request->description,
            'env_date' => $request->date,
            'evn_time' => $request->time,
            'evn_location' => $request->location,
            'evn_slotavaiable' => $request->avaliable,
            'evn_price' => $request->price,
            'evn_sts' => 1,
        ]);

        return redirect('dashboard')->with('toast_success','Successfuly Update Event ' . $request->title);
    }

    function destroy($id){
        mg_event::where('evn_id',Crypt::decrypt($id))->delete();

        return redirect('dashboard')->with('toast_success','Successfuly Delete Event ');
    }
}
