<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Mg_event;
use App\Models\Mg_booking;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
Use Alert;

class DashboardController extends Controller
{
    public function __construct(){
        // $this->middleware('auth:api');
    }

    public function index(){
        $data['title'] = "Dashboard | Booking App";
        // Check if there is an expiration date
        $checkactive = DB::table('mg_event as e')
                    ->where('e.env_date', '<', now())
                    ->where('e.evn_sts',1)
                    ->get();
        if(!empty($checkactive)){
            foreach($checkactive as $num => $row){
                mg_event::where('evn_id',$row->evn_id)->update(['evn_sts' => 2]);
            }
        }
        $title = 'Delete User!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);
        if (auth()->user()->role == 1) { // role 1 as Administrator
            $data['a1'] = DB::table('mg_event as e')
                        ->join('users as u', 'u.id', '=', 'e.evn_author')
                        ->orderBy('env_date','desc')
                        ->paginate(10);
            $data['upcoming'] = mg_event::whereBetween(DB::raw('DATE(env_date)'), 
                        [now(), now()->addDays(10)])
                        ->orderBy('env_date')
                        ->get();
        }else{
            $data['a1'] = DB::table('mg_event as e')
                        ->join('users as u', 'u.id', '=', 'e.evn_author')
                        ->orderBy('env_date','desc')
                        ->where('evn_author', auth()->user()->id)
                        ->paginate(10);
            $data['upcoming'] = mg_event::whereBetween(DB::raw('DATE(env_date)'), 
                        [now(), now()->addDays(10)])
                        ->where('evn_author', auth()->user()->id)
                        ->orderBy('env_date')
                        ->get();
        }
        return view('dashboard')->with('data',$data);
    }
}
