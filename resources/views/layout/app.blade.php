<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $data['title'] }}</title>
    @vite(['resources/sass/app.scss','resources/js/app.js'])
    <!-- @TODO: replace SET_YOUR_CLIENT_KEY_HERE with your client key -->
    <script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="{{config('midtrans.client_key')}}"></script>
    <!-- Note: replace with src="https://app.midtrans.com/snap/snap.js" for Production environment -->
</head>
<body>
    <div class="container">
        @include('sweetalert::alert')
        @if (Auth::check())
            @include('layout/navigate')
        @endif
        @include('layout/message')
        @yield('content')
    </div>
</body>
</html>