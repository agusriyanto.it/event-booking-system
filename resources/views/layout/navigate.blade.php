<header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
    <a href="/dashboard" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-body-emphasis text-decoration-none">
        <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
        <span class="fs-4">Event Booking System</span>
    </a>
    <ul class="nav nav-pills">
        <li class="nav-item"></li>
        <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown">
              {{auth()->user()->name; }}
            </button>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/profile">Profile</a></li>
                <li><a class="dropdown-item" href="/book">List Booking</a></li>
                <li><a class="dropdown-item" href="{{ route('reset-password')}}">Reset Password</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a href="/logout" class="dropdown-item">Logout</a></li>
            </ul>
          </div>
    </ul>
</header>