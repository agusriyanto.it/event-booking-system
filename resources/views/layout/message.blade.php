@if (Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show mt-5" role="alert">
        {{ Session::get('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
@if (Session::get('errorlogin'))
    <div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
        {{ Session::get('errorlogin') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
@if (Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
        {{ Session::get('err') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif