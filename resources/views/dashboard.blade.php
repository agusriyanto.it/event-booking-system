@extends('layout/app')
@section('content')
    <div class="row">
        @if (auth()->user()->usr_phone == null)
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    Sorry, your profile is not complete, please complete your profile first  <a href="/profile" class="alert-link">Complete Profile</a>.
                </div>
            </div>
        @else
        <div class="col-md-9">
            <a href="/event" >Add Event</a>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Status</th>
                            <th scope="col">Avaliable</th>
                            @php ; 
                                if (auth()->user()->role == 1) {  // role 1 as Administrator 
                            @endphp
                                <th scope="col">Create By</th>
                            @php ; } @endphp
                            <th scope="col">Action</th>                        
                        </tr>
                    </thead>
                    <tbody>
                        @php $no = 1; @endphp
                        @foreach ($data['a1'] as $num => $item)
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{$item->evn_title }}</td>
                                <td>{{$item->evn_description }}</td>
                                <td><?php if($item->evn_sts == 1) { ?> <span class='text-success' >Running</span><?php } else { ?><span class='text-danger' >Expired</span> <?php } ?></td>
                                <td>{{$item->evn_slotavaiable }}</td>
                                @php ; 
                                    if (auth()->user()->role == 1) {  // role 1 as Administrator 
                                @endphp
                                    <td>{{$item->name }}</td>
                                @php ; } @endphp
                                <td>
                                    <a href="{{ url('event/'.Crypt::encrypt($item->evn_id).'/edit') }}"  class="btn btn-info btn-sm">Edit</a>
                                    <a href="{{ url('book/'.Crypt::encrypt($item->evn_id).'/edit') }}"  class="btn btn-success btn-sm">Booking</a>
                                    <a href="{{ url('event/'.Crypt::encrypt($item->evn_id)) }}"  class="btn btn-primary btn-sm">Detail</a>
                                    <form class="d-inline" onsubmit="return confirm('are you sure delete this event ?');" action="{{ '/event/'. Crypt::encrypt($item->evn_id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Del</button>
                                    </form>
                                </td>                                
                            </tr>
                        @endforeach                        
                    </tbody>
                </table>
                {{ $data['a1']->links() }}
            </div>
        </div>
        <div class="col-md-3">
            <h3 class="text-dark fw-bold text-decoration-underline">Upcoming Events</h3>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    @if (isset($data['upcoming']))
                        <table>
                            <span class="text-dark mb-5 fw-bold">Found events in the next 10 days :</span>
                            @foreach ($data['upcoming'] as $num0 => $row0)
                            <tr>
                                <td>~ </td>
                                <td>{{ $row0->evn_title }}</td>
                                <td></td>
                                <td>
                                    <a href="{{ url('event/'.Crypt::encrypt($row0->evn_id)) }}" >Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    @else
                        <h6 class="text-dark">There are no events nearby</h6>
                    @endif
                    
                </div>
            </div>
        </div>
        @endif            
    </div>
@endsection