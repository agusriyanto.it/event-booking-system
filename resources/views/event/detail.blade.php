@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card shadow">
                <div class="card-title border-bottom">
                    <h2 class="p-3">Detail Event: <span class="fw-bold"> {{ $data['a1']->evn_title}}</span></h2>
                    <h6 class="p-3"><a href="/dashboard">Back</a></h6>
                </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td scope="row">Description</td>
                            <td>{{ $data['a1']->evn_description}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Date</td>
                            <td>{{ $data['a1']->env_date}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Time</td>
                            <td>{{ $data['a1']->evn_time}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Slot Avaliable</td>
                            <td>{{ $data['a1']->evn_slotavaiable}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Location</th>
                            <td>{{ $data['a1']->evn_location}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Price</th>
                            <td>{{ $data['a1']->evn_price}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="{{ url('book/'.Crypt::encrypt($data['a1']->evn_id).'/edit') }}"  class="btn btn-success btn-sm">Booking</a> </td>
                        </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection