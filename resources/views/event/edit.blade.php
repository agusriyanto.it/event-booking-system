@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card shadow">
                <div class="card-title border-bottom">
                    <h2 class="p-3">Edit Event</h2>
                    <h6 class="p-3"><a href="/dashboard">Back</a></h6>
                </div>
            <div class="card-body">
                <form action="{{'/event/'.$data['a1']->evn_id}}" method="POST" >
                    @csrf
                    @method('PUT')
                    <div class="mb-4">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" value="{{ $data['a1']->evn_title }}" id="title"/>
                        @error('title')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="description" class="form-label">Description</label>
                        <textarea type="text" class="form-control" name="description" id="description">{{ $data['a1']->evn_description }}</textarea>
                        @error('description')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="date" class="form-label">Date</label>
                        <input type="date" class="form-control" name="date" value="{{ $data['a1']->env_date }}" id="date"/>
                        @error('date')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="time" class="form-label">Time</label>
                        <input type="time" class="form-control" name="time" value="{{ $data['a1']->evn_time }}" id="time"/>
                        @error('time')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="location" class="form-label">Location</label>
                        <input type="text" class="form-control" name="location" value="{{ $data['a1']->evn_location }}" id="location"/>
                        @error('location')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="avaliable" class="form-label">Avaliable</label>
                        <input type="number" class="form-control" name="avaliable" value="{{ $data['a1']->evn_slotavaiable }}" id="avaliable"/>
                        @error('avaliable')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="price" class="form-label">Price</label>
                        <input type="number" class="form-control" name="price" value="{{ $data['a1']->evn_price }}" id="price"/>
                        @error('price')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="d-grid">
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection