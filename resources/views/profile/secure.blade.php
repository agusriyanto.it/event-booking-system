@extends('layout/app')
@section('content')
<style>
    .img {
        border-radius: 50%;
    }
    @-webkit-keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }

    @-moz-keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }

    @keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }

    .overlay-page-wrapper--animation
    {
        background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
        background-size: 400% 400%;
        opacity: .8;
        -webkit-animation: Gradient 15s ease infinite;
        -moz-animation: Gradient 15s ease infinite;
        animation: Gradient 15s ease infinite;
        z-index: 3;
        position: absolute;
        height: 100%;
        width: 100%;
        border-radius: 10px;
    }
    label.cabinet{
        display: block;
        cursor: pointer;
    }

    label.cabinet input.file{
        position: absolute;
        height: 0%;
        width: 0%;
        opacity: 0;
        -moz-opacity: 0;
        filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top:-130px;
    }

    #upload-demo{
        width: 100%;
        height: 350px;
        padding-bottom:25px;
    }
    #upload-demo-bg{
        width: 100%;
        height: 350px;
        padding-bottom:25px;
    }
    figure figcaption {
        position: absolute;
        top: 20px;
        color: #fff;
        width: 100%;
        padding-left: 65px;
        text-shadow: 0 0 10px #000;
        font-size: 20px;
    }

    label.bg{
        cursor: pointer;
        font-size: 20px;
        color: white;
        top: 0;
        right: 0;   
        margin-right: 15px;
        margin-top: 10px;
        position: absolute;
        text-shadow: 0 0 10px #000;
    }

    label.bg input.file{
        position: absolute;
        height: 0%;
        width: 0%;
        opacity: 0;
        -moz-opacity: 0;
        filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top:-130px;
    }
</style>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card border-0 m-0" style="border-radius: 15px;">
                    <img class="card-img img-fluid bg" src="{{url('/image/bg-default.jpg')}}" id="cover-output" alt="Card image" style="z-index: 4; border-radius: 15px; height: 160px">
                    <div class="card-img-overlay d-flex align-items-center justify-content-center flex-column mb-5"  style="height: 160px;z-index: 5;">
                        <div style="background: black; position: absolute; min-width: 100%; height: 100%; opacity: 0.5; border-radius: 15px"></div>
                        <form id="form-upload-bg">
                            <label class="bg" title="Upload Cover" data-toggle="tooltip" data-placement="left">
                                <i class="fa fa-images"></i>
                                <input type="file" class="files-cover file" style="cursor: pointer"/>
                                <input type="hidden" class="set-cover" value="" name="cover"/>
                            </label>
                        </form>
                    </div>
                    <form id="form-upload" style="margin-top:-100px;z-index: 8" method="post" action="" >
                        <div class="row text-center">
                            <div class="col-lg-12">
                                <label class="cabinet" title="Upload Photo">
                                    <figure>
                                        <figcaption class="label-rounded"><i class="fa fa-camera" data-toggle="tooltip" data-placement="right" title="Upload Foto"></i></figcaption>
                                    </figure>                        
                                    <input type="file" class="files-photo file" style="cursor: pointer"/>
                                    <input type="hidden" class="set-photo" value="" name="photo"/>
                                </label>
                                <div style="width:130px;height:130px;border-radius: 50%;border:6px solid gainsboro;margin:0px auto;overflow: hidden;vertical-align: middle">
                                    <img src="{{url('/image/img_avatar.png')}}" class="load-img" id="photo-output" style="width:100%" />    
                                </div>
                            </div>
                            <div class="col-lg-12 text-dark m-t-10">
                                <h3 class="text-dark font-bold">{{ $data['user']->name }}</h3>
                                <p>
                                    Registered
                                    <span class="font-bold">{{ date('d-M-Y', strtotime($data['user']->created_at)) }}</span>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8 mb-5">
                <h4>Reset Password</h4>
                <hr style="
                height: 8px;
                width: 100px;
                border-radius: 75px 75px;
                top: 10%;
                margin-bottom: 50px;
                background: green;"/>
                <form action="{{'/secure-action'}}" method="POST" >
                    @csrf
                    @method('POST')
                    <div class="mb-4">
                        <label for="name" class="form-label">New Password</label>
                        <input type="password" class="form-control"name="password" id="name"/>
                        @error('password')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="email" class="form-label">Confirm Password</label>
                        <input type="password" class="form-control" name="password_confirmation" id="email"/>
                        @error('password_confirmation')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    
                    <div class="d-grid">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection