@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-danger" role="alert">
                <h4>Your account is not active, please check your email to activate your account.!!!</h4> 
            </div>
            <small class="mt-5 mb-3">Sorry, Email activation under development, please click send activation</small>
            <h6>If you haven't received the activation email or the link has expired, <a href="{{ route('resend-mail') }}">send activation</a>.</h6>
        </div>
    </div>
@endsection