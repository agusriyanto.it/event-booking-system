@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card shadow">
                <div class="card-title border-bottom">
                    <h2 class="p-3">Detail Event {{ $data['a1']->evn_title}}</h2>
                    <h6 class="p-3"><a href="/book">Back</a></h6>
                </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">Description</th>
                            <td>{{ $data['a1']->evn_description}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Date</th>
                            <td>{{ $data['a1']->env_date}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Time</th>
                            <td>{{ $data['a1']->evn_time}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Slot Avaliable</th>
                            <td>{{ $data['a1']->evn_slotavaiable}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Location</th>
                            <td>{{ $data['a1']->evn_location}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Booking code</th>
                            <td><span class="text-dark font-bold">{{ $data['a1']->booking_code }}</span></td>
                        </tr>
                        <tr>
                            <th scope="row">Payment Status</th>
                            <td><h4 class="text-dark font-bold">{{ $data['a1']->booking_sts == 1 ? 'UNPAID' : 'Successfuly Payment' }}</h4></td>
                        </tr>
                        
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
@endsection