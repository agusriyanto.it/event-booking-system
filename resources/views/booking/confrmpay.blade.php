@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card shadow">
                <div class="card-title border-bottom text-center">
                    <h5 class="p-3">Payment Confirmation {{ $data['a1']->evn_title}}  </h5>
                    <h2 class="p-3 text-dark font-bold">{{ $data['a1']->booking_code }}</h2>
                </div>
            <div class="card-body">
                <table class="table">
                    <a href="/book">Back</a>
                    <tbody>
                        <tr>
                            <td scope="row"><h4>Description</h4></td>
                            <td>{{ $data['a1']->evn_description}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Date</td>
                            <td>{{ $data['a1']->env_date}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Time</td>
                            <td>{{ $data['a1']->evn_time}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Location</td>
                            <td>{{ $data['a1']->evn_location}}</td>
                        </tr>
                        <tr>
                            <td scope="row"></td>
                            <td><h4>{{ $data['a1']->evn_price}}</h4></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2"><button id="pay-button" class="btn btn-success btn-sm">Pay Now</button> </td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        // For example trigger on button clicked, or any time you need
        var payButton = document.getElementById('pay-button');
        payButton.addEventListener('click', function () {
          // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
          window.snap.pay('{{ $snapToken }}', {
            onSuccess: function(result){
              /* You may add your own implementation here */
              // alert("payment success!");
              window.location.href = '/book' 
              console.log(result);
            },
            onPending: function(result){
              /* You may add your own implementation here */
              alert("wating your payment!"); console.log(result);
            },
            onError: function(result){
              /* You may add your own implementation here */
              alert("payment failed!"); console.log(result);
            },
            onClose: function(){
              /* You may add your own implementation here */
              alert('you closed the popup without finishing the payment');
            }
          })
        });
      </script>
@endsection