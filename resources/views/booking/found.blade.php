@extends('layout/app')
@section('content')
    <div class="row justify-content-center mt-5">
        <header>
            <div class="d-flex flex-column flex-md-row align-items-center pb-3 mb-4 border-bottom">
              <a href="/" class="d-flex align-items-center link-body-emphasis text-decoration-none">
                <span class="fs-4">Booking App</span>
              </a>
            </div>
        
            <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
                <h1 class="display-4 fw-normal text-body-emphasis">Checkout Booking Code : {{ $data['a1']->booking_code }}</h1>
                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $data['a1']->usr_gender == 1 ? 'Mr. ' : 'Ms. ' }} {{ $data['a1']->name }}</td>
                            <td>{{ $data['a1']->usr_address }}</td>
                            <td>{{ $data['a1']->email }}</td>
                            <td>{{ $data['a1']->booking_sts == 1 ? 'Unpaid' : 'Paid' }}</td>
                            <td>
                                @if ($data['a1']->booking_sts == 2)
                                    <button id="pay-button" class="btn btn-success btn-sm">Print</button>
                                @else
                                <button id="pay-button" class="btn btn-secondary btn-sm" disabled>Print</button>
                                @endif
                            </td>
                        </tr>
                        <tr>
                    </tbody>
                </table>
            </div>
          </header>
    </div>
@endsection