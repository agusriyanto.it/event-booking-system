@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-lg-12 col-md-12">
            <div class="card shadow">
                <div class="card-title border-bottom">
                    <h2 class="p-3">Booking Event</h2>
                    <h6 class="p-3"><a href="/dashboard">Back</a></h6>
                </div>
            <div class="card-body">
                <form action="{{'/book'}}" method="POST" >
                    @csrf
                    @method('POST')
                    <input type="hidden" name="id" value="{{$data['a1']->evn_id}}"/>
                    <input type="hidden" name="title" value="{{$data['a1']->evn_title}}"/>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Avaliable</th>                     
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                    <tr>
                                        <td>{{$data['a1']->evn_title }}</td>
                                        <td>{{$data['a1']->evn_description }}</td>
                                        <td>{{$data['a1']->env_date }}</td>
                                        <td>{{$data['a1']->evn_time }}</td>
                                        <td>{{$data['a1']->evn_location }}</td>
                                        <td>{{$data['a1']->evn_slotavaiable }}</td>                                
                                    </tr>                      
                            </tbody>
                        </table>
                    </div>
                    <div class="d-grid">
                        
                            @if ($data['check'] == 1)
                                <button type="submit" class="btn btn-secondary" disabled>Full Booked</button>
                            @else
                                @if ($data['checkuser'] > 0)
                                    <span class="text-danger">You have already made a booking at this event..!!!</span>                                    
                                @else
                                    @if ($data['exp'] != 2)
                                        <button type="submit" class="btn btn-primary">Booking</button>
                                    @endif
                                @endif                                
                            @endif
                            @if ($data['exp'] == 2)
                                <span class="text-danger">This event has expired..!!!</span>
                            @endif
                            
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection