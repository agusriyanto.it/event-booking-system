@extends('layout/app')
@section('content')
    <div class="row justify-content-center mb-5">
        @if (auth()->user()->usr_phone == null)
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    Sorry, your profile is not complete, please complete your profile first  <a href="/profile" class="alert-link">Complete Profile</a>.
                </div>
            </div>
        @else
        <div class="col-lg-12 col-md-12">
            <div class="card shadow">
                <div class="card-title border-bottom">
                    <h2 class="p-3">Booking Event</h2>
                    <h6 class="p-3"><a href="/dashboard">Back</a></h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Booking Code</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Status</th>
                                    @php ; 
                                        if (auth()->user()->role == 1) {  // role 1 as Administrator 
                                    @endphp
                                        <th scope="col">
                                        By  
                                        </th>
                                @php ; } @endphp
                                    <th scope="col">Action</th>                     
                                </tr>
                            </thead>
                            <tbody class="align-midle">
                                @php $no = 1; @endphp
                                @foreach ($data['a1'] as $num => $item)
                                    <tr>
                                        <td class="text-center">
                                            {{$item->booking_code }} <br />
                                            
                                            {!! QrCode::size(65)->generate(route('check-code',['id' => $item->booking_code])) !!}
                                        </td>
                                        <td>{{$item->evn_title }}</td>
                                        <td>{{$item->env_date }}</td>
                                        <td>{{$item->evn_time }}</td>
                                        <td>{{$item->evn_price }}</td>
                                        <td>
                                            <?php if ($item->booking_sts == 1) { ?>
                                                <span class="btn btn-info btn-sm">unpaid</span>
                                            <?php } elseif($item->booking_sts == 2) { ?>
                                                <span class="btn btn-success btn-sm">paid</span>
                                            <?php }else { ?>
                                                <span class="btn btn-danger btn-sm">Expired</span>
                                            <?php } ?>
                                        </td>
                                        @php ; 
                                            if (auth()->user()->role == 1) {  // role 1 as Administrator 
                                        @endphp
                                        <td>{{$item->name }}</td>
                                        @php ; 
                                            }
                                        @endphp
                                        <td>
                                            <a href="{{ url('book/'.Crypt::encrypt($item->booking_id)) }}"  class="btn btn-success btn-sm">Detail</a>
                                            <?php if ($item->booking_sts == 1) { ?>
                                                <a href="{{ route('confrm-pay',['id' => Crypt::encrypt($item->booking_id)]) }}"  class="btn btn-warning btn-sm">Pay</a>
                                            <?php } ?>
                                        </td>                                
                                    </tr>
                                @endforeach                       
                            </tbody>
                        </table>
                        {{ $data['a1']->links() }}
                    </div>
                </div>
            </div>
        </div>
        @endif
            
    </div>
@endsection